# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 09:02:34 2016

@author: Administrator
"""

import reza_helpers as reza_hp
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from random import randint
from time import sleep
import os , subprocess


#==============================================================================
# ===== Get links from data base table for files on s3 bucket
query = "SELECT links FROM filenames_laall_logs WHERE field_name='Bandini' AND Name_file LIKE '%DATA%'"

conn,cursor = reza_hp.establish_DBconn()
cursor.execute(query)
s3bucket_links = cursor.fetchall()






#==============================================================================
DownloadDir  = "C:\Users\Administrator\Desktop\Reza Projects\LAS WebCrawling\Downloads"
S3BucketName = "ndk-logs"
MainDir      = os.getcwd()




#==============================================================================
chromeOptions = webdriver.ChromeOptions()
prefs         = {"download.default_directory" : DownloadDir}
chromeOptions.add_experimental_option("prefs",prefs)
chromedriver = "C:\Anaconda2\selenium\webdriver\chrome\chromedriver.exe"


#==== Connect to the webpage
mydriver = webdriver.Chrome(executable_path=chromedriver, chrome_options=chromeOptions)


#======== Log in and save the coockie
mydriver.get("https://<user>:<pass>@www.dmr.nd.gov/oilgas/feeservices/getlogs.asp")
#============================================================================== 
   
   
#====== find Fields elements and iterat through them
select_Field  = mydriver.find_element_by_name("ddmField")
options_Field = select_Field.find_elements_by_tag_name("option") 




for i in range(50,len(options_Field)):
    
    # Find the elements
    select_Field  = mydriver.find_element_by_name("ddmField")
    options_Field = select_Field.find_elements_by_tag_name("option") 
    option        = options_Field[i]
    # find submit button
    submit_btn    = mydriver.find_element_by_xpath('/html/body/table/tbody/tr/td[2]/form/p[4]/input[1]')
    
    
    print "Option Number: ( "+str(i)+" ) For Field [" + str(option.text) + "]"
    # add a random sleep time to avoide being blocked
    
    
    # select the option
    option.click()
    submit_btn.click()
    sleep(10)
    
    # Find LAS files in the page
    list_links = mydriver.find_elements_by_css_selector("a[href*='/oilgas/feeservices/dlogs/']")
    
    
    
    for link in list_links[1:2]:
        # add a random sleep time to avoide being blocked
        sleep(randint(1,5))
      
        # Perform ALT+Click on the link to automatically download it
        try:
            ActionChains(mydriver).key_down(Keys.ALT).click(link).perform()
            ActionChains(mydriver).key_up(Keys.ALT).perform() # release the alt key          
        except:
            print "Error On"

            
    #====== Now push the downloaded files in S3 bucket
    os.chdir(DownloadDir)
    sleep(1)
    
    #subprocess.call("aws s3 sync . s3://" + S3BucketName) # transfer files in current directory to s3 bucket
    os.system("aws s3 sync . s3://ndk-logs")
    subprocess.call("del /s *.* 2>nul")
    os.chdir(MainDir)  

            
    




# ======== Close the driver and browser    
mydriver.quit()        
     




