# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 17:26:03 2016

@author: navjot
"""

import sys
from math import sin, cos, sqrt, atan2, radians
import MySQLdb



#function to calculate distance between points
def dist(lon1,lat1, lon2, lat2):
    """Auxiliary function to calculate distance in miles between a 
    pair of coordinates using Haversine method
    @param coord1,coord2 - Pair of Coordinates"""
    R= 3961 # Earth radius in Miles
    dlat=radians(abs((lat2)-(lat1)))
    dlon=radians(abs((lon2)-(lon1)))
   
    a = (sin(dlat / 2) * sin(dlat / 2)) + (cos(radians(lat2)) * cos(radians(lat1)) * (sin(dlon / 2) * sin(dlon / 2)))
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    
    distance = R * c
    return distance



#connection to database
cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com','user':'navjot_limit','password': 'forgotit','db': 'public_data'}
conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
cursor = conn.cursor()


# change source table name for state in query
query = "Select API_master, BH_longi,BH_lat, WH_longi, WH_lat from public_data.utah_master where BH_lat is not null and WH_lat is not null;"
cursor.execute(query)
row = cursor.fetchall()
wellID=[]
distance = []
for r in row:
    distance.append(dist(r[1], r[2], r[3], r[4]))
    wellID.append(r[0])
    
#function to create table    
def create_table(conn,cursor,schema,table_name):
   schema='public_data'
   query = "CREATE TABLE IF NOT EXISTS `"+schema+"`.`"+table_name+"` (\
  `API_master` bigint(20) NOT NULL,`Distance` double DEFAULT NULL,PRIMARY KEY (`API_master`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;"
   cursor.execute(query)
    
   query2 = "TRUNCATE "+schema+"."+table_name+";"
   cursor.execute(query2)
   return
  
#give table name to create in database  
create_table(conn, cursor,"public_data", "utah_distance")

joined_file = zip(wellID,distance) #binding calculated distance and API


#function to write in created table in database
def writeDB(conn,cursor,schema,table_name,output_file):
    schema='public_data'
    query = ("INSERT INTO`"+schema+"`.`"+table_name+"`(`API_master`,`Distance`)\
    VALUES (%s,%s);")
    
    for i in xrange(len(output_file)):
        cursor.execute(query,tuple(output_file[i]))
        conn.commit()
    conn.close()

# write in already created table
writeDB(conn, cursor, "public_data", "utah_distance", joined_file)
