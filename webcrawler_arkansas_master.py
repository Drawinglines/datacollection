# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 14:48:37 2016

@author: navjot
"""

import os
from selenium import webdriver
from selenium.webdriver.support.ui import Select
#from selenium.common.exceptions import NoSuchElementException
#import MySQLdb
#from selenium.webdriver.common.keys import Keys
from time import sleep
#from selenium.common.exceptions import TimeoutException  
#from selenium.common.exceptions import NoSuchElementException
#from selenium.common.exceptions import StaleElementReferenceException


chromeOptions = webdriver.ChromeOptions()
#foldernames=["1b","1c"]
#fnpointer=-1
prefs = {"download.default_directory" : "C:/Users/Administrator/Downloads/Arkansas/master"}
chromeOptions.add_experimental_option("prefs",prefs)
chromedriver = "C:\Users\Administrator\Downloads\chromedriver_win32/chromedriver.exe"
os.environ["webdriver.chrome.driver"] = chromedriver
mydriver = webdriver.Chrome(executable_path=chromedriver, chrome_options=chromeOptions)
mydriver.get("http://www.aogc2.state.ar.us/welldata/Wells/default.aspx")
sleep(10)
select = mydriver.find_element_by_id("cpMainContent_ddlCriteria")
select.find_element_by_xpath("//*[@id='cpMainContent_ddlCriteria']/option[6]").click()
sleep(10)
select = mydriver.find_element_by_xpath( "//*[@id='cpMainContent_ddlListItem']")  #get the select element 
sleep(5)           
options = select.find_elements_by_tag_name("option") #get all the options into a list
sleep(10)
optionsList = []

for option in options: #iterate over the options, place attribute value in list
    optionsList.append(option.get_attribute("value"))

sleep(10)

for optionValue in optionsList:
    select = mydriver.find_element_by_id("cpMainContent_ddlCriteria")
    select.find_element_by_xpath("//*[@id='cpMainContent_ddlCriteria']/option[6]").click()
    sleep(10)
    select = mydriver.find_element_by_xpath("//*[@id='cpMainContent_ddlListItem']")
    Select(mydriver.find_element_by_css_selector('#cpMainContent_ddlListItem')).select_by_value(optionValue)
    sleep(10)
    searchelement = mydriver.find_element_by_xpath("//*[@id='cpMainContent_btnSubmit']")
    searchelement.click()
    sleep(200)
    exportelement = mydriver.find_element_by_xpath("//*[@id='cpMainContent_btnExcel']")
    exportelement.click()
    sleep(800)