# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 15:51:30 2016

@author: navjot
"""

import os
from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
from selenium.webdriver.support.ui import Select
import sys

#from selenium.webdriver.support.ui import Select, WebDriverWait
#from selenium.common.exceptions import NoSuchElementException
#import MySQLdb
#from selenium.webdriver.common.keys import Keys
from time import sleep
#from selenium.common.exceptions import TimeoutException  
#from selenium.common.exceptions import NoSuchElementException
#from selenium.common.exceptions import StaleElementReferenceException


chromeOptions = webdriver.ChromeOptions()
#foldernames=["1b","1c"]
#fnpointer=-1
prefs = {"download.default_directory" : "/Users/navjot/Desktop/Data collection/Arkansas/Production data"}
chromeOptions.add_experimental_option("prefs",prefs)
chromedriver = "C:\Users\Administrator\Downloads\chromedriver_win32/chromedriver.exe"
os.environ["webdriver.chrome.driver"] = chromedriver
mydriver = webdriver.Chrome(chromedriver)
mydriver = webdriver.Chrome(executable_path=chromedriver, chrome_options=chromeOptions)
mydriver.get("http://www.aogc2.state.ar.us/welldata/Production/Default.aspx")
sleep(10)

select = mydriver.find_element_by_id("cpMainContent_ddlCriteria")
select.find_element_by_xpath("//*[@id='cpMainContent_ddlCriteria']/option[4]").click()
sleep(10)

select = mydriver.find_element_by_id("cpMainContent_ddlBegins")
select.find_element_by_xpath("//*[@id='cpMainContent_ddlBegins']/option[5]").click()
sleep(10)

inputElement = mydriver.find_element_by_id("cpMainContent_txtBegins")
inputElement.send_keys('0')
sleep(10)

searchelement = mydriver.find_element_by_id("cpMainContent_btnSubmit")
searchelement.click()
sleep(20)

select = mydriver.find_element_by_xpath("//*[@id='cpMainContent_ddlItem']")
select.find_element_by_xpath("//*[@id='cpMainContent_ddlItem']/option[4]").click()
sleep(10)

select = mydriver.find_element_by_xpath( "//*[@id='cpMainContent_ddlPermit']")  #get the select element 
sleep(10)           
options = select.find_elements_by_tag_name("option") #get all the options into a list
sleep(10)

reload(sys)
sys.setdefaultencoding('utf-8')

optionsList = []

for option in options: #iterate over the options, place attribute value in list
    optionsList.append(option.get_attribute("value"))

sleep(5)


count = 0
for optionValue in optionsList:
    select = mydriver.find_element_by_xpath("//*[@id='cpMainContent_ddlPermit']")
    Select(mydriver.find_element_by_css_selector('#cpMainContent_ddlPermit')).select_by_value(optionValue)
    sleep(15)
    html = mydriver.page_source
    sleep(2)
    count = count+1
    BeautifulSoup("html", "html.parser")
    soup = BeautifulSoup(html)
    table = soup.find_all('table')[4]
    rows = table.find_all('tr')[2:]
    data = {
        'lease_no' : [],
        'report_date' : [],
        'lease_name' : [],
        'oil' : [],
        'gas' : [],
        'water' : []
        }
        

    for row in rows:
        cols = row.find_all('td')
        data['lease_no'].append( cols[0].get_text() )
        data['report_date'].append( cols[1].get_text() )
        data['lease_name'].append( cols[2].get_text() )
        data['oil'].append( cols[3].get_text() )
        data['gas'].append( cols[4].get_text() )
        data['water'].append( cols[5].get_text() )
   
    aa = pd.DataFrame(data)
    aa = aa.replace(to_replace = '\n', value = '', regex = True)
    aa['api'] = optionValue
    aa.to_csv(optionValue)
    if count%100 == 0:
        print count , optionValue
