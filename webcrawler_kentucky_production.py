# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 13:39:21 2016

@author: navjot
"""

from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.support.ui import Select

firefoxProfile = FirefoxProfile()
## Disable CSS
firefoxProfile.set_preference('permissions.default.stylesheet', 2)
## Disable images
firefoxProfile.set_preference('permissions.default.image', 2)
## Disable Flash
firefoxProfile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so',
                              'false')
mydriver = webdriver.Firefox(firefoxProfile)
mydriver.get("http://kgs.uky.edu/kgsmap/OGProdPlot/OGProduction.asp")
change=mydriver.find_element_by_xpath("/html/body/div/div/div[3]/form/div/div/div/div[1]/input[2]")
change.click()
select = Select(mydriver.find_element_by_id("areatype2"))
select.select_by_visible_text("Quadrangle")
select = mydriver.find_element_by_xpath( "//*[@id='quadname']")            
options = select.find_elements_by_tag_name("option") 
optionsList = []
for option in options:
    optionsList.append(option.get_attribute("value"))
for optionValue in optionsList:
    select = Select(mydriver.find_element_by_xpath( "//*[@id='quadname']"))
    select.select_by_value(optionValue)
    sumbitButtonElem = mydriver.find_element_by_xpath('//*[@id="wellDownload"]/input') 
    sumbitButtonElem.click()