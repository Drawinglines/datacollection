# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 11:39:36 2016

@author: navjot
"""

import os
from selenium import webdriver
#from selenium.webdriver.common.keys import Keys
from time import sleep

chromedriver = "/Users/navjot/Desktop/Navjot/Python Scripts/chromedriver"
os.environ["webdriver.chrome.driver"] = chromedriver

mydriver = webdriver.Chrome(chromedriver)
mydriver.get("http://bogc.dnrc.mt.gov/webapps/dataminer/Wells/WellMultiSearch.aspx")

select = mydriver.find_element_by_xpath("//*[@id='ctl00_SiteContentPlaceHolder_FormFilterNoNav1_cboFilterOption']")
select.find_element_by_xpath("//*[@id='ctl00_SiteContentPlaceHolder_FormFilterNoNav1_cboFilterOption']/option[7]").click()
sleep(100)

optionsalpha = list(map(chr, range(97, 123)))

for options in optionsalpha:
    inputElement = mydriver.find_element_by_id("ctl00_SiteContentPlaceHolder_FormFilterNoNav1_txtFilter")
    inputElement.send_keys(options)
    sleep(10)
    searchElement = mydriver.find_element_by_id("ctl00_SiteContentPlaceHolder_FormFilterNoNav1_btnSearch")
    searchElement.click()
    sleep(10)
    excelElement = mydriver.find_element_by_id("ctl00_SiteContentPlaceHolder_FormFilterNoNav1_webCtrlExport1_btnExcel")
    excelElement.click()
    sleep(10)
