# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 14:58:29 2016

@author: navjot
"""

import os
from selenium import webdriver
import MySQLdb
#from selenium.webdriver.common.keys import Keys
from time import sleep
#from selenium.common.exceptions import TimeoutException  
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException



 #Connecting to database   
cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com','user':'navjot_limit','password': 'forgotit','db': 'public_data'}
conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
cursor = conn.cursor()

query = "SELECT distinct API_master from montana_master order by API_master;"
cursor.execute(query)
row = cursor.fetchall()
wellID=[]
for r in row:
    wellID.append(r[0])
    
sleep(100)     

#changing the download folder to paralelize the process
chromeOptions = webdriver.ChromeOptions()
foldernames=["1b","1c"]
fnpointer=-1
prefs = {"download.default_directory" : "C:/Users/Administrator/Downloads/Montana prod/1a"}
chromeOptions.add_experimental_option("prefs",prefs)
chromedriver = "C:\Users\Administrator\Downloads\chromedriver_win32/chromedriver.exe"
os.environ["webdriver.chrome.driver"] = chromedriver
mydriver = webdriver.Chrome(executable_path=chromedriver, chrome_options=chromeOptions)
mydriver.get("http://bogc.dnrc.mt.gov/webapps/dataminer/Production/ProductionByWell.aspx")



#optionsalpha = list(map(chr, range(97, 123)))

api = wellID[1:9000] #total APIs are 44481, ran 6 webcrawlers to make the process fast as website of montana oil and gas data is slow
apiwexcept1 = []
apiwnoelement1 = []
count = 0
for options in api:
    if count%3001==0:
        mydriver.close()
        fnpointer+=1
        chromeOptions = webdriver.ChromeOptions()
        prefs["download.default_directory"]="C:/Users/Administrator/Downloads/Montana prod/"+foldernames[fnpointer%len(foldernames)]
        chromeOptions.add_experimental_option("prefs",prefs)
        chromedriver = "C:\Users\Administrator\Downloads\chromedriver_win32/chromedriver.exe"
        os.environ["webdriver.chrome.driver"] = chromedriver
        mydriver = webdriver.Chrome(executable_path=chromedriver, chrome_options=chromeOptions)
        mydriver.get("http://bogc.dnrc.mt.gov/webapps/dataminer/Production/ProductionByWell.aspx")
        sleep(10)
        select = mydriver.find_element_by_xpath("//*[@id='ctl00_SiteContentPlaceHolder_FormFilterWithNav1_cboFilterType']")
        select.find_element_by_xpath("//*[@id='ctl00_SiteContentPlaceHolder_FormFilterWithNav1_cboFilterType']/option[2]").click()
        sleep(10)
    
    inputElement = mydriver.find_element_by_id("ctl00_SiteContentPlaceHolder_FormFilterWithNav1_txtFilter")
    inputElement.send_keys(options)
    sleep(1)
    searchElement = mydriver.find_element_by_id("ctl00_SiteContentPlaceHolder_FormFilterWithNav1_btnSearch")
    searchElement.click()
    sleep(1)
    if count%1000 == 0:
        print count
    try:
        thirdElement = mydriver.find_element_by_id("ctl00_SiteContentPlaceHolder_FormFilterWithNav1_Navigation1_dvTreeViewt2")
        thirdElement.click()
        thirdElement.click()
        sleep(1)
        excelElement = mydriver.find_element_by_id("ctl00_SiteContentPlaceHolder_FormFilterWithNav1_webCtrlExport1_btnExcel")
        excelElement.click()
        sleep(1)
        
    except NoSuchElementException:
        apiwnoelement1.append(api)
        sleep(1)
    
    except StaleElementReferenceException:
        apiwexcept1.append(api)
        sleep(1)
    count = count+1
                 