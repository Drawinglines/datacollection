# -*- coding: utf-8 -*-
"""
Created on Sat Jan 23 10:58:46 2016

@author: navjot
"""

from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.support.ui import Select

firefoxProfile = FirefoxProfile()
## Disable CSS
firefoxProfile.set_preference('permissions.default.stylesheet', 2)
## Disable images
firefoxProfile.set_preference('permissions.default.image', 2)
## Disable Flash
firefoxProfile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so',
                              'false')
#mydriver = webdriver.Chrome(chromeProfile)
mydriver = webdriver.Firefox(firefoxProfile)
mydriver.get("https://apps.dep.wv.gov/oog/wellsearch_new.cfm")
#change=mydriver.find_element_by_xpath("/html/body/div/div/div[3]/form/div/div/div/div[1]/input[2]")
#change.click()
#select = Select(mydriver.find_element_by_id("areatype2"))
#select.select_by_visible_text("Quadrangle")
select = mydriver.find_element_by_xpath("//*[@id='pickForm']/table/tbody/tr[15]/td/input")
select.click()
select = mydriver.find_element_by_xpath( "//*[@id='county']")            
options = select.find_elements_by_tag_name("option") 
optionsList = []
for option in options:
    optionsList.append(option.get_attribute("value"))
for optionValue in optionsList:
    select = Select(mydriver.find_element_by_xpath( "//*[@id='county']"))
    select.select_by_value(optionValue)
    sumbitButtonElem = mydriver.find_element_by_xpath("//*[@id='pickForm']/table/tbody/tr[17]/td/input[1]") 
    sumbitButtonElem.click()